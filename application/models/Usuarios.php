<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Model{



    public function __construct(){
        $this->load->database();
   }

   public function getPost(){
    $this->db->query("SELECT * FROM usuarios");

    return $result->row();
}

    public function actualizar(int $id,string $nombre,string $apellido,string $telefono){
        $this->db->where('id', $id);

        $data  = array( 
            'nombre' => $nombre,
            'apellido' => $apellido,
            'telefono' => $telefono
        );
    
        $this->db->update('usuarios',$data);


}
    public function eliminar(int $id){
        $this->db->where('id', $id);
        $this->db->delete('usuarios');
    }

    public function getById(int $id){
        $query = $this->db->get_where('usuarios', array('id' => $id));
    
        if ($query->result()) {
            return $query->result();
          } else {
            return false;
          }
       
    }

    
}