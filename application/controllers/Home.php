<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

   function __construct(){

    parent::__construct();


  }


	  public function index(){
        $result = $this->db->get('usuarios');
        $data = array('consulta'=>$result);
		    $this->load->view('home',$data);
    }

   	public function cargarEdicion(int $id){
		$info = $this->Usuarios->getById($id);
		
		$this->load->view('ModificarUsuario',array('info' => $info));
   }

    public function editar()
	{
		if($this->input->post())
		{
			$id = $this->input->post("id");
			$nombre = $this->input->post("nombre");
			$apellido = $this->input->post("apellido");
			$telefono = $this->input->post("telefono");
			$this->Usuarios->actualizar($id, $nombre, $apellido, $telefono);
			
		}
	}

	public function eliminar(int $id)
	{
	 $this->Usuarios->eliminar($id);
			
	 header("Location:".base_url()."Home");
			
	}

     
    

    
    
    
}
